/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import Build from '../util/build.js'
import { BuildPowerSets } from '../enums/BuildPowerSets.js'
import { getPowerLevel } from '../enums/PowerLevel.js'
import { PowerLevel } from '../enums/PowerLevel.js'
import { PowerSetType } from '../enums/PowerSetType.js'
import Toon from '../util/toon.js'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    state: {
      statusMessages: [],
      errorMessages: [],
      toon: null,
      toonMetadata: {
        primaryPowerCount: 0,
        secondaryPowerCount: 0,
        pool1PowerCount: 0,
        pool2PowerCount: 0,
        pool3PowerCount: 0,
        pool4PowerCount: 0,
        epicPowerCount: 0,
      },
      powerNames: [],
      dbArchetypesData: {},
      dbEffectsData: {},
      dbEnhancementSetsData: {},
      dbEnhancementsData: {},
      dbPowerSetsData: {},
      dbPowersData: {},
      dbLoaded: false
    },
    mutations: {
      addErrorMessage (state, errorMessage) {
        state.errorMessages.push(errorMessage)
      },
      addStatusMessage (state, statusMessage) {
        state.statusMessages.push(statusMessage)
      },
      removeOldestErrorMessage (state) {
        if (state.errorMessages.length > 0) {
          state.errorMessages.shift()
        }
      },
      removeOldestStatusMessage (state) {
        if (state.statusMessages.length > 0) {
          state.statusMessages.shift()
        }
      },
      setDBArchetypes (state, archetypes) {
        state.dbArchetypesData = archetypes
      },
      setDBEffects (state, effects) {
        state.dbEffectsData = effects
      },
      setDBEnhancements (state, enhancements) {
        state.dbEnhancementsData = enhancements
      },
      setDBEnhancementSets (state, enhancementSets) {
        state.dbEnhancementSetsData = enhancementSets
      },
      setDBLoaded (state, loaded) {
        state.dbLoaded = loaded
      },
      setDBPowers (state, powers) {
        state.dbPowersData = powers
      },
      setDBPowerSets (state, powerSets) {
        state.dbPowerSetsData = powerSets
      },
      setPowerName (state, powerIdAndName) {
        state.powerNames.push(powerIdAndName)
      },
      toonCreate (state, archetype_id) {
        let toon = Toon.from_archetype(archetype_id)
        state.toon = toon
      },
      toonSelectBuild (state, buildNumber) {
        if (!state.toon)
          return
        let toon = state.toon
        let build = toon.builds[toon.current_build]
        while (buildNumber > toon.builds.length - 1) {
          let newBuild = Build.from_archetype(toon.archetype_id)
          newBuild.power_sets[BuildPowerSets.primary] = build.power_sets[BuildPowerSets.primary]
          newBuild.power_sets[BuildPowerSets.secondary] = build.power_sets[BuildPowerSets.secondary]
    
          if (2 < toon.level) {
            Build.add_level_2_powers(newBuild)
          }  
          toon.builds.push(newBuild)
        }
        toon.current_build = buildNumber
        build = toon.builds[toon.current_build]
        if (2 < toon.level) {
          Build.add_level_2_powers(build)
        }  
      },
      toonSetArchetypeName (state, name) {
        if (!state.toon)
          return
        state.toon_archetype_name = name
      },
      toonSetLevel (state, level) {
        if (!state.toon)
          return
        state.toon.level = level
      },
      toonSetName (state, name) {
        if (!state.toon)
          return
        state.toon.name = name
      },
      toonSetPowerEntry (state, payload) {
        if (!state.toon)
          return
        let toon = state.toon
        let build = state.toon.builds[state.toon.current_build]
        build.power_entries[payload.buildPowerLevel] = payload.powerEntry

        if (payload.powerEntry.level > toon.level)
          toon.level = payload.powerEntry.level
        
        if (2 < toon.level) {
          Build.add_level_2_powers(build)
        }

        let power = state.dbPowersData[payload.powerEntry.power_id]
        if (power) {
          let powerSet = state.dbPowerSetsData[power.power_set_id]
          if (powerSet) {
            if (powerSet.set_type === 'Pool') {
              if (build.power_sets[BuildPowerSets.pool_1] !== powerSet.id
              && build.power_sets[BuildPowerSets.pool_2] !== powerSet.id
              && build.power_sets[BuildPowerSets.pool_3] !== powerSet.id
              && build.power_sets[BuildPowerSets.pool_4] !== powerSet.id) {
                if (!build.power_sets[BuildPowerSets.pool_1])
                  build.power_sets[BuildPowerSets.pool_1] = powerSet.id
                else if (!build.power_sets[BuildPowerSets.pool_2])
                  build.power_sets[BuildPowerSets.pool_2] = powerSet.id
                else if (!build.power_sets[BuildPowerSets.pool_3])
                  build.power_sets[BuildPowerSets.pool_3] = powerSet.id
                else if (!build.power_sets[BuildPowerSets.pool_4])
                  build.power_sets[BuildPowerSets.pool_4] = powerSet.id
              }
            } else if (powerSet.set_type === 'Ancillary') {
              if (build.power_sets[BuildPowerSets.epic] !== powerSet.id) {
                build.power_sets[BuildPowerSets.epic] = powerSet.id
              }
            }
          }
        }
        state.toon = toon

        let primary = build.power_sets.primary
        let secondary = build.power_sets.secondary
        let pool_1 = build.power_sets.pool_1
        let pool_2 = build.power_sets.pool_2
        let pool_3 = build.power_sets.pool_3
        let pool_4 = build.power_sets.pool_4
        let epic = build.power_sets.epic
        let toonMetadata = {
          primaryPowerCount: 0,
          secondaryPowerCount: 0,
          pool1PowerCount: 0,
          pool2PowerCount: 0,
          pool3PowerCount: 0,
          pool4PowerCount: 0,
          epicPowerCount: 0,
        }

        for (let powerEntryIndex = 0; powerEntryIndex < build.power_entries.length; powerEntryIndex++) {
          let powerEntry = build.power_entries[powerEntryIndex]
          if (!powerEntry || !powerEntry.power_id)
            continue
          let powerSet = state.dbPowerSetsData[powerEntry.power_id]
          if (!powerSet)
            continue
          if (powerSet.id === primary)
            toonMetadata.primaryPowerCount++
          else if (powerSet.id === secondary)
            toonMetadata.secondaryPowerCount++
          else if (powerSet.id === pool_1)
            toonMetadata.pool1PowerCount++
          else if (powerSet.id === pool_2)
            toonMetadata.pool2PowerCount++
          else if (powerSet.id === pool_3)
            toonMetadata.pool3PowerCount++
          else if (powerSet.id === pool_4)
            toonMetadata.pool4PowerCount++
          else if (powerSet.id === epic)
            toonMetadata.epicPowerCount++
        }
        state.toonMetadata = toonMetadata
      },
      toonSetPrimaryPowerSet (state, power_set_id) {
        if (!state.toon)
          return
        let build = state.toon.builds[state.toon.current_build]
        build.power_sets.primary = power_set_id
      },
      toonSetSecondaryPowerSet (state, power_set_id) {
        if (!state.toon)
          return
        let build = state.toon.builds[state.toon.current_build]
        build.power_sets.secondary = power_set_id
      },
      toonUpdate (state, toon) {
        state.toon = toon
      },
    },
    actions: {
      loadDatabase (context) {
        let archetypes = require('../database/archetypes.map.json')
        // let effects = require('../database/effects.map.json')
        let enhancementSets = require('../database/enhancement-sets.map.json')
        let enhancemments = require('../database/enhancements.map.json')
        let powerSets = require('../database/power-sets.map.json')
        let powers = require('../database/powers.map.json')

        context.commit('setDBArchetypes', archetypes)
        // context.commit('setDBEffects', effects)
        context.commit('setDBEnhancementSets', enhancementSets)
        context.commit('setDBEnhancements', enhancemments)
        context.commit('setDBPowerSets', powerSets)
        context.commit('setDBPowers', powers)
        setTimeout(function(){ context.commit('setDBLoaded', true) }, 3000);
      }
    },
    getters: {
      errorMessageCount: state => {
        return state.errorMessages.length
      },
      getArchetype: (state, getters) => (id) => {
        return state.dbArchetypesData[id]
      },
      getDBArchetypes: state => {
        return state.dbArchetypesData
      },
      getDBEffects: state => {
        return state.dbEffectsData
      },
      getDBEnhancementSets: state => {
        return state.dbEnhancementSetsData
      },
      getDBEnhancements: state => {
        return state.dbEnhancementsData
      },
      getDBPowerSets: state => {
        return state.dbPowerSetsData
      },
      getDBPowers: state => {
        return state.dbPowersData
      },
      getDBLoaded: state => {
        return state.dbLoaded
      },
      getEffect: (state, getters) => (id) => {
        return state.dbEffectsData[id]
      },
      getEnhancementSet: (state, getters) => (id) => {
        return state.dbEnhancementSetsData[id]
      },
      getEnhancement: (state, getters) => (id) => {
        return state.dbEnhancementsData[id]
      },
      getEpicPowerSetsFromArchetype: (state, getters) => (id) => {
        let archetype = getters.getArchetype(id)
        let powerSets = []
        if (!archetype || !archetype.powersets_ancillary)
          return powerSets

        archetype.powersets_ancillary.forEach((powerSetId) => {
          let powerSet = getters.getPowerSet(powerSetId)
          if (!powerSet)
            return
            powerSets.push(powerSet)
        })
        return powerSets
      },
      getPoolPowerSetsFromArchetype: (state, getters) => (id) => {
        let powerSets = []
        let dbPowerSets = getters.getDBPowerSets
        _.forIn(dbPowerSets, function(powerSet, powerSetId) {
          if (!powerSet.id.startsWith("Pool."))
            return;
          if ("Pool.Leadership_beta" == powerSet.id)
              return;
          if ("Class_Warshade" == id || "Class_Peacebringer" == id) {
            if ("Pool.Flight" == powerSet.id || "Pool.Teleportation" == powerSet.id) {
                return;
            }
          }
          powerSets.push(powerSet)
        });
        return powerSets
      },
      getPower: (state, getters) => (id) => {
        return state.dbPowersData[id]
      },
      getPowerName: (state, getters) => (powerId) => {
        let mapped = state.powerNames.map(x => (x.powerId === powerId)? x : null)
        if (0 == mapped.length) return null
        let reduced = mapped.reduce((accumulator, currentValue) => (currentValue)? currentValue : accumulator)
        if (!reduced) return null
        return reduced.powerName
      },
      getPowerNames: state => {
        return state.powerNames
      },
      getPowerSet: (state, getters) => (id) => {
        return state.dbPowerSetsData[id]
      },
      getPowersFromPowerSet: (state, getters) => (id) => {
        let powerSets = getters.getDBPowerSets
        let powerSet = powerSets[id]
        let power_ids = powerSet.power_ids
        let getPower = (powerId) => { return state.dbPowersData[powerId] }
        let powers = power_ids.map(getPower)
        return powers
      },
      getPrimaryPowerSetsFromArchetype: (state, getters) => (id) => {
        let archetype = getters.getArchetype(id)
        let powerSets = []
        if (!archetype || !archetype.powersets_primary)
          return powerSets

        archetype.powersets_primary.forEach((powerSetId) => {
          let powerSet = getters.getPowerSet(powerSetId)
          if (!powerSet)
            return
            powerSets.push(powerSet)
        })
        return powerSets
      },
      getSecondaryPowerSetsFromArchetype: (state, getters) => (id) => {
        let archetype = getters.getArchetype(id)
        let powerSets = []
        if (!archetype || !archetype.powersets_secondary)
          return powerSets

        archetype.powersets_secondary.forEach((powerSetId) => {
          let powerSet = getters.getPowerSet(powerSetId)
          if (!powerSet)
            return
            powerSets.push(powerSet)
        })
        return powerSets
      },
      getToon: state => {
        return state.toon
      },
      getToonAllowedPowers: (state, getters) => (payload) => {
        let powers = getters.getPowersFromPowerSet(payload.powerSetId)
        let powers_meet_requirements = []

        powers.forEach((power) => {
          if (power.level > payload.powerLevel)
            return
          if (getters.getToonHasPower(power.id)) 
              return
          if (!getters.getToonMeetsPowerRequirements(power))
              return
          powers_meet_requirements.push(power);
        })

        return powers_meet_requirements
      },
      getToonAllowedPowerSets: (state, getters) => (power_level_enum) => {
        let toon = getters.getToon
        let build = getters.getToonCurrentBuild
        let power_level = getPowerLevel(power_level_enum, toon.archetype_id)
        let power_sets = []
        if (!build.power_sets[BuildPowerSets.primary]
        || !build.power_sets[BuildPowerSets.secondary]) {
            return power_sets
        }

        let primary_id = build.power_sets[BuildPowerSets.primary]
        let primary = getters.getPowerSet(primary_id)
        let secondary_id = build.power_sets[BuildPowerSets.secondary]
        let secondary = getters.getPowerSet(secondary_id)
        if (power_level_enum !== PowerLevel.level_1_secondary
        && !getters.getToonHasAllPowersForSet(primary_id)) {
            power_sets.push(primary);
        }
        if (power_level_enum !== PowerLevel.level_1_primary 
        && !getters.getToonHasAllPowersForSet(secondary_id)) {
            power_sets.push(secondary);
        }

        if (power_level_enum !== PowerLevel.level_1_primary
        && power_level_enum !== PowerLevel.level_1_secondary
        && power_level_enum !== PowerLevel.level_2) {
            let pool_1_id = build.power_sets[BuildPowerSets.pool_1]
            let pool_2_id = build.power_sets[BuildPowerSets.pool_2]
            let pool_3_id = build.power_sets[BuildPowerSets.pool_3]
            let pool_4_id = build.power_sets[BuildPowerSets.pool_4]
            if (!pool_1_id
            || !pool_2_id
            || !pool_3_id
            || !pool_4_id) {
                let pools = getters.getPoolPowerSetsFromArchetype(toon.archetype_id)
                pools.forEach((powerSet) => {
                    if (!getters.getToonHasAllPowersForSet(powerSet)) {
                        power_sets.push(powerSet);
                    }
                })
            } else {
                let pool_1 = getPowerSet(pool_1_id)
                let pool_2 = getPowerSet(pool_2_id)
                let pool_3 = getPowerSet(pool_3_id)
                let pool_4 = getPowerSet(pool_4_id)
                if (!getters.getToonHasAllPowersForSet(pool_1.power_ids)) {
                    power_sets.push(pool_1);
                }
                if (!getters.getToonHasAllPowersForSet(pool_2.power_ids)) {
                    power_sets.push(pool_2);
                }
                if (!getters.getToonHasAllPowersForSet(pool_3.power_ids)) {
                    power_sets.push(pool_3);
                }
                if (!getters.getToonHasAllPowersForSet(pool_4.power_ids)) {
                    power_sets.push(pool_4);
                }
            }
        }

        if (power_level >= 35) {
            if (!build.power_sets[BuildPowerSets.epic]) {
                let epic_power_sets = getters.getEpicPowerSetsFromArchetype(toon.archetype_id)
                epic_power_sets.forEach((epic_power_set) => {
                  if (!getters.getToonHasAllPowersForSet(epic_power_set.id)) {
                    let epic = getters.getPowerSet(epic_power_set.id)
                    power_sets.push(epic);
                  }
                })
            } else {
                let epic_id = build.power_sets[BuildPowerSets.epic]
                if (!getters.getToonHasAllPowersForSet(epic_id)) {
                  let epic = getters.getPowerSet(epic_id)
                  power_sets.push(epic);
                }
            }
        }

        let power_sets_meet_requirements = []
        power_sets.forEach((power_set) => {
          let powers = getters.getPowersFromPowerSet(power_set.id)
          let powers_meet_requirements = []

          if (power_level_enum === PowerLevel.level_1_primary
          && power_set.set_type !== PowerSetType.PRIMARY) {
            return;
          }
          if (power_level_enum === PowerLevel.level_1_secondary
          && power_set.set_type !== PowerSetType.SECONDARY) {
              return;
          }

          powers.forEach((power) => {
            if (power.level > power_level) {
              return;
            }
            if (!getters.getToonMeetsPowerRequirements(power)) {
              return
            }
            powers_meet_requirements.push(power)
          }) 
          if (0 === powers_meet_requirements.length) {
            return;
          }

          power_sets_meet_requirements.push(power_set);
        })
        return power_sets_meet_requirements
      },
      getToonArchetypeId: state => {
        if (!state.toon)
          return null
        return state.toon.archetype_id
      },
      getToonCurrentBuild: state => {
        if (!state.toon)
          return null
        return state.toon.builds[state.toon.current_build]
      },
      getToonCurrentBuildNumber: state => {
        if (!state.toon)
          return null
        return state.toon.current_build
      },
      getToonHasAllPowersForSet: (state, getters) => (powerSetId) => {
        let build = getters.getToonCurrentBuild
        let powerSet = getters.getDBPowerSets[powerSetId]
        let primary = build.power_sets.primary
        let secondary = build.power_sets.secondary
        let pool_1 = build.power_sets.pool_1
        let pool_2 = build.power_sets.pool_2
        let pool_3 = build.power_sets.pool_3
        let pool_4 = build.power_sets.pool_4
        let epic = build.power_sets.epic
        if (powerSetId === primary) {
          return state.toonMetadata.primaryPowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === secondary) {
          return state.toonMetadata.secondaryPowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === pool_1) {
          return state.toonMetadata.pool1PowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === pool_2) {
          return state.toonMetadata.pool2PowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === pool_3) {
          return state.toonMetadata.pool3PowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === pool_4) {
          return state.toonMetadata.pool4PowerCount === powerSet.power_ids.length
        }
        else if (powerSetId === epic) {
          return state.toonMetadata.epicPowerCount === powerSet.power_ids.length
        }
        return false
      },     
      getToonHasPower: (state, getters) => (powerId) => {
        return !!getters.getToonPowerEntryFromPowerId(powerId)
      },
      getToonLevel: state => {
        if (!state.toon)
          return null
        return state.toon.level
      },
      getToonMeetsPowerRequirements: (state, getters) => (power) => {
        let meets = true;
        for (let requiresIndex = 0; requiresIndex < power.requires.power_id.length; requiresIndex++) {
            let or_list = power.requires.power_id[requiresIndex]
            let and_meets = true;
            for (let orIndex = 0; orIndex < or_list.length; orIndex++) {
                let and_power_id = or_list[orIndex]
                if ('' === and_power_id)
                    continue;
                if (!getters.getToonPowerEntryFromPowerId(and_power_id)) {
                    and_meets = false;
                    break;
                }
            }
            meets = and_meets;
            if (meets)
                break;
        }
        return meets
      },
      getToonName: state => {
        if (!state.toon)
          return null
        return state.toon.name
      },
      getToonPowerEntryFromPowerId: (state, getters) => (powerId) => {
        let build = getters.getToonCurrentBuild
        let powerEntry = _.find(build.power_entries, function(powerEntry) { return powerEntry.power_id === powerId; });
        return powerEntry
      },
      getToonSortedPowerEntries: state => {
        if (!state.toon)
          return null
        const powerEntries = state.toon.builds[state.toon.current_build].power_entries
        const level = state.toon.level
        let sortedPowerEntries = []
        sortedPowerEntries.push({ level: 'level_1_primary', power_entry: powerEntries.level_1_primary })
        sortedPowerEntries.push({ level: 'level_1_secondary', power_entry: powerEntries.level_1_secondary})
        sortedPowerEntries.push({ level: 'level_2', power_entry: powerEntries.level_2})
        sortedPowerEntries.push({ level: 'level_4', power_entry: powerEntries.level_4})
        sortedPowerEntries.push({ level: 'level_6', power_entry: powerEntries.level_6})
        sortedPowerEntries.push({ level: 'level_8', power_entry: powerEntries.level_8})
        sortedPowerEntries.push({ level: 'level_10', power_entry: powerEntries.level_10})
        sortedPowerEntries.push({ level: 'level_12', power_entry: powerEntries.level_12})
        sortedPowerEntries.push({ level: 'level_14', power_entry: powerEntries.level_14})
        sortedPowerEntries.push({ level: 'level_16', power_entry: powerEntries.level_16})
        sortedPowerEntries.push({ level: 'level_18', power_entry: powerEntries.level_18})
        sortedPowerEntries.push({ level: 'level_20', power_entry: powerEntries.level_20})
        sortedPowerEntries.push({ level: 'level_22', power_entry: powerEntries.level_22})
        sortedPowerEntries.push({ level: 'level_24', power_entry: powerEntries.level_24})
        sortedPowerEntries.push({ level: 'level_26', power_entry: powerEntries.level_26})
        sortedPowerEntries.push({ level: 'level_28', power_entry: powerEntries.level_28})
        sortedPowerEntries.push({ level: 'level_30', power_entry: powerEntries.level_30})
        sortedPowerEntries.push({ level: 'level_32', power_entry: powerEntries.level_32})
        sortedPowerEntries.push({ level: 'level_35', power_entry: powerEntries.level_35})
        sortedPowerEntries.push({ level: 'level_38', power_entry: powerEntries.level_38})
        sortedPowerEntries.push({ level: 'level_41', power_entry: powerEntries.level_41})
        sortedPowerEntries.push({ level: 'level_44', power_entry: powerEntries.level_44})
        sortedPowerEntries.push({ level: 'level_47', power_entry: powerEntries.level_47})
        sortedPowerEntries.push({ level: 'level_49', power_entry: powerEntries.level_49})
        sortedPowerEntries.push({ level: 'health', power_entry: powerEntries.health})
        sortedPowerEntries.push({ level: 'stamina', power_entry: powerEntries.stamina})
        sortedPowerEntries.push({ level: 'brawl', power_entry: powerEntries.brawl})
        sortedPowerEntries.push({ level: 'prestige_power_slide', power_entry: powerEntries.prestige_power_slide})
        if (2 <= level) sortedPowerEntries.push({ level: 'sprint', power_entry: powerEntries.sprint})
        if (2 <= level) sortedPowerEntries.push({ level: 'hurdle', power_entry: powerEntries.hurdle})
        if (2 <= level) sortedPowerEntries.push({ level: 'swift', power_entry: powerEntries.swift})
        if ('Class_Peacebringer' === state.toon.archetype_id || 'Class_Warshade' === state.toon.archetype_id) {
          for (let buildIndex = 0; buildIndex < toon.builds.length; buildIndex++) {
            let build = toon.builds[buildIndex]
            sortedPowerEntries.push({ level: 'level_1_kheldian', power_entry: powerEntries.level_1_kheldian})
            if (10 <= level) sortedPowerEntries.push({ level: 'level_10_kheldian', power_entry: powerEntries.level_10_kheldian})
            if (null !== powerEntries.nova_1) sortedPowerEntries.push({ level: 'nova_1', power_entry: powerEntries.nova_1})
            if (null !== powerEntries.nova_2) sortedPowerEntries.push({ level: 'nova_2', power_entry: powerEntries.nova_2})
            if (null !== powerEntries.nova_3) sortedPowerEntries.push({ level: 'nova_3', power_entry: powerEntries.nova_3})
            if (null !== powerEntries.nova_4) sortedPowerEntries.push({ level: 'nova_4', power_entry: powerEntries.nova_4})
            if (null !== powerEntries.dwarf_1) sortedPowerEntries.push({ level: 'dwarf_1', power_entry: powerEntries.dwarf_1})
            if (null !== powerEntries.dwarf_2) sortedPowerEntries.push({ level: 'dwarf_2', power_entry: powerEntries.dwarf_2})
            if (null !== powerEntries.dwarf_3) sortedPowerEntries.push({ level: 'dwarf_3', power_entry: powerEntries.dwarf_3})
            if (null !== powerEntries.dwarf_4) sortedPowerEntries.push({ level: 'dwarf_4', power_entry: powerEntries.dwarf_4})
            if (null !== powerEntries.dwarf_5) sortedPowerEntries.push({ level: 'dwarf_5', power_entry: powerEntries.dwarf_5})
            if (null !== powerEntries.dwarf_6) sortedPowerEntries.push({ level: 'dwarf_6', power_entry: powerEntries.dwarf_6})
          }
        }
        let filteredPowerEntries = _.filter(sortedPowerEntries, function(pe, index, collection) { 
          if (!pe.power_entry)
            return true
          return pe.power_entry.level <= level 
        });
        return filteredPowerEntries      
      },
      hasPowerName: (state, getters) => (powerId) => {
        let mapped = state.powerNames.map(x => (x.powerId === powerId)? x : null)
        if (0 == mapped.length) return false
        let reduced = mapped.reduce((accumulator, currentValue) => (currentValue)? currentValue : accumulator)
        if (!reduced) return false
        return !!reduced.powerName
      },
      statusMessageCount: state => {
        return state.statusMessages.length
      },
    }
  })

  return Store
}
