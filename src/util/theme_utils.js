const setThemeLight = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#EEEEEF')
  style.setProperty('--theme_background_color_menu_bar', '#EEEEEF')
  style.setProperty('--theme_background_color_status_bar', '#EEEEEF')
  style.setProperty('--theme_background_color_button_down', '#555555')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#0000FF')
  style.setProperty('--theme_text_color_label', '#555555')
  style.setProperty('--theme_text_color_field', '#555555')
  style.setProperty('--theme_text_color_button_down', '#EEEEEF')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid white')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#555555')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#003399')
  style.setProperty('--theme_secondary_power_set', '#990000')
  style.setProperty('--theme_pool_power_set', '#FF9900')
  style.setProperty('--theme_epic_power_set', '#FFFF66')
  style.setProperty('--theme_inherent_power_set', '#336600  ')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeDark = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#555555')
  style.setProperty('--theme_background_color_menu_bar', '#555555')
  style.setProperty('--theme_background_color_status_bar', '#555555')
  style.setProperty('--theme_background_color_button_down', '#EEEEEF')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#0000FF')
  style.setProperty('--theme_text_color_label', '#EEEEEF')
  style.setProperty('--theme_text_color_field', '#EEEEEF')
  style.setProperty('--theme_text_color_button_down', '#555555')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid white')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#555555')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#6495ED')
  style.setProperty('--theme_secondary_power_set', '#FF4500')
  style.setProperty('--theme_pool_power_set', '#FFA500')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#3CB371')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeHero = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#003366')
  style.setProperty('--theme_background_color_menu_bar', '#003366')
  style.setProperty('--theme_background_color_status_bar', '#003366')
  style.setProperty('--theme_background_color_button_down', '#FFCC66')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#FFCC66')
  style.setProperty('--theme_text_color_label', '#FFCC66')
  style.setProperty('--theme_text_color_field', '#FFCC66')
  style.setProperty('--theme_text_color_button_down', '#003366')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid FFCC00')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#003366')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#000080')
  style.setProperty('--theme_secondary_power_set', '#FF2400')
  style.setProperty('--theme_pool_power_set', '#FF9900')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#00CC00')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeVillian = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#810000')
  style.setProperty('--theme_background_color_menu_bar', '#810000')
  style.setProperty('--theme_background_color_status_bar', '#810000')
  style.setProperty('--theme_background_color_button_down', '#f5efef')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#f5efef')
  style.setProperty('--theme_text_color_label', '#f5efef')
  style.setProperty('--theme_text_color_field', '#f5efef')
  style.setProperty('--theme_text_color_button_down', '#810000')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid FFCC00')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#810000')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#07689f')
  style.setProperty('--theme_secondary_power_set', '#FF2400')
  style.setProperty('--theme_pool_power_set', '#FFA500')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#3CB371')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeGamma = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#2C6700')
  style.setProperty('--theme_background_color_menu_bar', '#2C6700')
  style.setProperty('--theme_background_color_status_bar', '#2C6700')
  style.setProperty('--theme_background_color_button_down', '#FFCF79')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#FFCF79')
  style.setProperty('--theme_text_color_label', '#FFCF79')
  style.setProperty('--theme_text_color_field', '#FFCF79')
  style.setProperty('--theme_text_color_button_down', '#2C6700')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid FFCF79')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#2C6700')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#003399')
  style.setProperty('--theme_secondary_power_set', '#FF2400')
  style.setProperty('--theme_pool_power_set', '#FF9900')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#00CC00')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setTheme3D = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#2C6700')
  style.setProperty('--theme_background_color_menu_bar', '#2C6700')
  style.setProperty('--theme_background_color_status_bar', '#2C6700')
  style.setProperty('--theme_background_color_button_down', '#FFCF79')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#FFCF79')
  style.setProperty('--theme_text_color_label', '#FFCF79')
  style.setProperty('--theme_text_color_field', '#FFCF79')
  style.setProperty('--theme_text_color_button_down', '#2C6700')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid FFCF79')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#2C6700')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', 'none')
  style.setProperty('--theme_box_shadow_image_button', 'none')
  style.setProperty('--theme_box_shadow_image_button_active', 'none')
  style.setProperty('--theme_box_shadow_small', 'none')
  style.setProperty('--theme_box_shadow_small_active', 'none')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#003399')
  style.setProperty('--theme_secondary_power_set', '#FF2400')
  style.setProperty('--theme_pool_power_set', '#FF9900')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#00CC00')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeNeumorphic = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#EEEEEF')
  style.setProperty('--theme_background_color_menu_bar', '#EEEEEF')
  style.setProperty('--theme_background_color_status_bar', '#EEEEEF')
  style.setProperty('--theme_background_color_button_down', '#EEEEEF')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#3000FF')
  style.setProperty('--theme_text_color_label', '#3000FF')
  style.setProperty('--theme_text_color_field', '#3000FF')
  style.setProperty('--theme_text_color_button_down', '#0055ff')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid white')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#555555')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', '5px 5px 5px #bababa, -5px -5px 5px #ffffff')
  style.setProperty('--theme_box_shadow_image_button', '5px 5px 5px #bababa, -5px -5px 5px #ffffff')
  style.setProperty('--theme_box_shadow_image_button_active', '5px 5px 5px #ffffff, -5px -5px 5px #bababa')
  style.setProperty('--theme_box_shadow_small', '3px 3px 3px #bababa, -3px -3px 3px #ffffff')
  style.setProperty('--theme_box_shadow_small_active', '3px 3px 3px #ffffff, -3px -3px 3px #bababa')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#6495ED')
  style.setProperty('--theme_secondary_power_set', '#FF4500')
  style.setProperty('--theme_pool_power_set', '#FFA500')
  style.setProperty('--theme_epic_power_set', '#FFFFBF')
  style.setProperty('--theme_inherent_power_set', '#3CB371')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

const setThemeNeumorphicDark = () => {
  const style = document.documentElement.style

  style.setProperty('--theme_background_color', '#242424')
  style.setProperty('--theme_background_color_menu_bar', '#242424')
  style.setProperty('--theme_background_color_status_bar', '#242424')
  style.setProperty('--theme_background_color_button_down', '#242424')
  style.setProperty('--theme_font_family', '"Roboto", sans-serif')
  style.setProperty('--theme_font_size_small', 'small')
  style.setProperty('--theme_font_size_medium', 'medium')
  style.setProperty('--theme_font_size_large', 'large')
  style.setProperty('--theme_text_align_title', 'left')
  style.setProperty('--theme_text_align_image_button', 'center')
  style.setProperty('--theme_text_align_label', 'left')
  style.setProperty('--theme_text_align_field', 'left')
  style.setProperty('--theme_text_color_image_button', '#7B68EE')
  style.setProperty('--theme_text_color_label', '#7B68EE')
  style.setProperty('--theme_text_color_field', '#7B68EE')
  style.setProperty('--theme_text_color_button_down', '#7B68EE')
  style.setProperty('--theme_text_color_image_button_disabled', '#999999')
  style.setProperty('--theme_text_color_label_disabled', '#999999')
  style.setProperty('--theme_text_color_field_disabled', '#999999')
  style.setProperty('--theme_text_color_slots_remaining', '#999999')
  style.setProperty('--theme_border_radius_image_button_image', '50%')
  style.setProperty('--theme_border_image_button_image', '2px solid black')
  style.setProperty('--theme_buton_text_color', '#EEEEEF')
  style.setProperty('--theme_buton_background_color', '#242424')
  style.setProperty('--theme_buton_text_color_disabled', '#999999')
  style.setProperty('--theme_buton_background_color_disabled', '#EEEEEF')
  style.setProperty('--theme_box_shadow_menu_bar', '5px 5px 5px #000000, -3px -3px 3px #7B68EE')
  style.setProperty('--theme_box_shadow_image_button', '5px 5px 5px #000000, -3px -3px 3px #7B68EE')
  style.setProperty('--theme_box_shadow_image_button_active', '5px 5px 5px #7B68EE, -3px -3px 3px #000000')
  style.setProperty('--theme_box_shadow_small', '3px 3px 3px #000000, -2px -2px 2px #7B68EE')
  style.setProperty('--theme_box_shadow_small_active', '2px 2px 2px #7B68EE, -3px -3px 3px #000000')
  style.setProperty('--theme_no_power_set', '#999999')
  style.setProperty('--theme_primary_power_set', '#000080')
  style.setProperty('--theme_secondary_power_set', '#FF2400')
  style.setProperty('--theme_pool_power_set', '#FFCC00')
  style.setProperty('--theme_epic_power_set', '#CCCC00')
  style.setProperty('--theme_inherent_power_set', '#00CC00')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
  style.setProperty('--theme_primary_power_set', '#003399')
  style.setProperty('--theme_secondary_power_set', '#990000')
  style.setProperty('--theme_pool_power_set', '#FF9900')
  style.setProperty('--theme_epic_power_set', '#FFFF66')
  style.setProperty('--theme_inherent_power_set', '#336600  ')
  style.setProperty('--theme_power_set_background_color', '#DDDDDD')
  style.setProperty('--theme_power_set_text_color', '#555555')
}

export { setThemeLight, setThemeDark, setThemeHero, setThemeVillian, setThemeGamma, setThemeNeumorphic, setThemeNeumorphicDark }
